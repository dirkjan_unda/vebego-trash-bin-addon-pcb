EESchema Schematic File Version 2
LIBS:Undagrid-vebego-addon-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:custom
LIBS:vl53l0x
LIBS:BQ51003YFPT
LIBS:dk_PMIC-Battery-Chargers
LIBS:Undagrid-vebego-addon-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "Trash bin Addon board"
Date "2018-07-25"
Rev "V0.2"
Comp "Undagrid"
Comment1 "Qi battery charger"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L INDUCTOR L201
U 1 1 5B0432A8
P 675 2250
F 0 "L201" V 625 2250 50  0000 C CNN
F 1 "INDUCTOR" V 775 2250 50  0000 C CNN
F 2 "Custom:Qi_coil" H 675 2250 50  0001 C CNN
F 3 "" H 675 2250 50  0000 C CNN
F 4 "not a part" H 675 2250 60  0001 C CNN "MPN"
F 5 "DNP" H 675 2250 60  0001 C CNN "Placement"
	1    675  2250
	-1   0    0    1   
$EndComp
$Comp
L C_Small C203
U 1 1 5B043399
P 1250 1900
F 0 "C203" H 1260 1970 50  0000 L CNN
F 1 "220n" H 1260 1820 50  0000 L CNN
F 2 "RCL:0603_CAP" H 1250 1900 50  0001 C CNN
F 3 "" H 1250 1900 50  0000 C CNN
F 4 "06033D224KAT2A" H 1250 1900 60  0001 C CNN "MPN"
F 5 "25V" H 1250 1900 60  0001 C CNN "Remarks"
	1    1250 1900
	0    1    1    0   
$EndComp
$Comp
L GND #PWR049
U 1 1 5B043E9F
P 5125 2500
F 0 "#PWR049" H 5125 2250 50  0001 C CNN
F 1 "GND" H 5125 2350 50  0000 C CNN
F 2 "" H 5125 2500 50  0000 C CNN
F 3 "" H 5125 2500 50  0000 C CNN
	1    5125 2500
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R205
U 1 1 5B044FA2
P 6400 2550
F 0 "R205" H 6430 2570 50  0000 L CNN
F 1 "820" H 6430 2510 50  0000 L CNN
F 2 "RCL:0603_RES" H 6400 2550 50  0001 C CNN
F 3 "" H 6400 2550 50  0000 C CNN
F 4 "MCWR06X8200FTL" H 6400 2550 60  0001 C CNN "MPN"
	1    6400 2550
	1    0    0    1   
$EndComp
$Comp
L GND #PWR050
U 1 1 5B0451C5
P 3300 3300
F 0 "#PWR050" H 3300 3050 50  0001 C CNN
F 1 "GND" H 3300 3150 50  0000 C CNN
F 2 "" H 3300 3300 50  0000 C CNN
F 3 "" H 3300 3300 50  0000 C CNN
	1    3300 3300
	0    1    1    0   
$EndComp
$Comp
L C_Small C212
U 1 1 5B045355
P 3125 2125
F 0 "C212" H 3135 2195 50  0000 L CNN
F 1 "10u" H 3135 2045 50  0000 L CNN
F 2 "RCL:0603_CAP" H 3125 2125 50  0001 C CNN
F 3 "" H 3125 2125 50  0000 C CNN
	1    3125 2125
	0    -1   -1   0   
$EndComp
$Comp
L R_Small R204
U 1 1 5B04540C
P 6100 2550
F 0 "R204" H 6130 2570 50  0000 L CNN
F 1 "196" H 6130 2510 50  0000 L CNN
F 2 "RCL:0603_RES" H 6100 2550 50  0001 C CNN
F 3 "" H 6100 2550 50  0000 C CNN
F 4 "ERJ3EKF1960V" H 6100 2550 60  0001 C CNN "MPN"
	1    6100 2550
	1    0    0    1   
$EndComp
$Comp
L R_Small R203
U 1 1 5B0454F6
P 5700 2275
F 0 "R203" H 5730 2295 50  0000 L CNN
F 1 "DNP" H 5730 2235 50  0000 L CNN
F 2 "RCL:0603_RES" H 5700 2275 50  0001 C CNN
F 3 "" H 5700 2275 50  0000 C CNN
F 4 "DNP" H 5700 2275 60  0001 C CNN "Placement"
	1    5700 2275
	1    0    0    -1  
$EndComp
$Comp
L C_Small C213
U 1 1 5B0459B3
P 5500 2975
F 0 "C213" H 5510 3045 50  0000 L CNN
F 1 "100n" H 5510 2895 50  0000 L CNN
F 2 "RCL:0603_CAP" H 5500 2975 50  0001 C CNN
F 3 "" H 5500 2975 50  0000 C CNN
F 4 "16V" H 5500 2975 60  0001 C CNN "Remarks"
	1    5500 2975
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR051
U 1 1 5B045C32
P 3300 2125
F 0 "#PWR051" H 3300 1875 50  0001 C CNN
F 1 "GND" H 3300 1975 50  0000 C CNN
F 2 "" H 3300 2125 50  0000 C CNN
F 3 "" H 3300 2125 50  0000 C CNN
	1    3300 2125
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR052
U 1 1 5B045C9D
P 5500 3150
F 0 "#PWR052" H 5500 2900 50  0001 C CNN
F 1 "GND" H 5500 3000 50  0000 C CNN
F 2 "" H 5500 3150 50  0000 C CNN
F 3 "" H 5500 3150 50  0000 C CNN
	1    5500 3150
	1    0    0    -1  
$EndComp
Text Label 6675 2400 0    60   ~ 0
FOD
Text Label 4950 3100 0    60   ~ 0
FOD
$Comp
L C_Small C204
U 1 1 5B043428
P 1475 2250
F 0 "C204" H 1485 2320 50  0000 L CNN
F 1 "5.6n" H 1485 2170 50  0000 L CNN
F 2 "RCL:0603_CAP" H 1475 2250 50  0001 C CNN
F 3 "" H 1475 2250 50  0000 C CNN
F 4 "CC0603KRX7R9BB562" H 1475 2250 60  0001 C CNN "MPN"
F 5 "25V" H 1475 2250 60  0001 C CNN "Remarks"
	1    1475 2250
	1    0    0    -1  
$EndComp
$Comp
L TEST J201
U 1 1 5B0914DD
P 975 2250
F 0 "J201" H 975 2310 50  0000 C CNN
F 1 "TEST" H 975 2180 50  0000 C CNN
F 2 "Custom:COIL_CON" H 975 2250 50  0001 C CNN
F 3 "" H 975 2250 50  0000 C CNN
F 4 "DNP" H 975 2250 60  0001 C CNN "Placement"
	1    975  2250
	0    1    1    0   
$EndComp
$Comp
L D D201
U 1 1 5B0C491C
P 8250 925
F 0 "D201" H 8250 1025 50  0000 C CNN
F 1 "D" H 8250 825 50  0000 C CNN
F 2 "Custom:SOD-123_custom" H 8250 925 50  0001 C CNN
F 3 "" H 8250 925 50  0000 C CNN
F 4 "MBR0520LT1G" H 8250 925 60  0001 C CNN "MPN"
F 5 "DNP" H 8250 925 60  0001 C CNN "Remarks"
	1    8250 925 
	-1   0    0    1   
$EndComp
$Comp
L C_Small C202
U 1 1 5B0CE767
P 1250 1575
F 0 "C202" H 1260 1645 50  0000 L CNN
F 1 "220n" H 1260 1495 50  0000 L CNN
F 2 "RCL:0603_CAP" H 1250 1575 50  0001 C CNN
F 3 "" H 1250 1575 50  0000 C CNN
F 4 "25V" H 1250 1575 60  0001 C CNN "Remarks"
	1    1250 1575
	0    1    1    0   
$EndComp
$Comp
L C_Small C201
U 1 1 5B0CE7BE
P 1250 1275
F 0 "C201" H 1260 1345 50  0000 L CNN
F 1 "100n" H 1260 1195 50  0000 L CNN
F 2 "RCL:0603_CAP" H 1250 1275 50  0001 C CNN
F 3 "" H 1250 1275 50  0000 C CNN
F 4 "06033D104KAT2A" H 1250 1275 60  0001 C CNN "MPN"
F 5 "25V" H 1250 1275 60  0001 C CNN "Remarks"
	1    1250 1275
	0    1    1    0   
$EndComp
$Comp
L C_Small C205
U 1 1 5B0CE808
P 1700 2250
F 0 "C205" H 1710 2320 50  0000 L CNN
F 1 "C" H 1710 2170 50  0000 L CNN
F 2 "RCL:0603_CAP" H 1700 2250 50  0001 C CNN
F 3 "" H 1700 2250 50  0000 C CNN
F 4 "DNP" H 1700 2250 60  0001 C CNN "Placement"
F 5 "25V" H 1700 2250 60  0001 C CNN "Remarks"
	1    1700 2250
	1    0    0    -1  
$EndComp
$Comp
L C_Small C208
U 1 1 5B0CEBB8
P 2000 1650
F 0 "C208" V 1950 1700 50  0000 L CNN
F 1 "10n" V 1950 1450 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 1650 50  0001 C CNN
F 3 "" H 2000 1650 50  0000 C CNN
F 4 "06031C103K4Z2A" H 2000 1650 60  0001 C CNN "MPN"
F 5 "25V" H 2000 1650 60  0001 C CNN "Remarks"
	1    2000 1650
	0    1    1    0   
$EndComp
$Comp
L C_Small C207
U 1 1 5B0CEBBE
P 2000 1425
F 0 "C207" V 1950 1475 50  0000 L CNN
F 1 "0.47u" V 1950 1175 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 1425 50  0001 C CNN
F 3 "" H 2000 1425 50  0000 C CNN
F 4 "06033C474KAT2A" H 2000 1425 60  0001 C CNN "MPN"
F 5 "25V" H 2000 1425 60  0001 C CNN "Remarks"
	1    2000 1425
	0    1    1    0   
$EndComp
$Comp
L C_Small C206
U 1 1 5B0CEBC4
P 2000 1200
F 0 "C206" V 1950 1250 50  0000 L CNN
F 1 "22n" V 1950 1025 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 1200 50  0001 C CNN
F 3 "" H 2000 1200 50  0000 C CNN
F 4 "06033C223KAT2A" H 2000 1200 60  0001 C CNN "MPN"
F 5 "25V" H 2000 1200 60  0001 C CNN "Remarks"
	1    2000 1200
	0    1    1    0   
$EndComp
Text Label 2275 1200 0    60   ~ 0
COM1
Text Label 2275 1425 0    60   ~ 0
CLMP1
Text Label 2275 1650 0    60   ~ 0
BOOT1
Text Label 2275 2850 0    60   ~ 0
BOOT2
Text Label 2275 3075 0    60   ~ 0
CLMP2
Text Label 2275 3300 0    60   ~ 0
COM2
Text Label 4950 2800 0    60   ~ 0
BOOT2
Text Label 4950 2900 0    60   ~ 0
CLMP2
Text Label 4950 3000 0    60   ~ 0
COM2
Text Label 3275 3000 2    60   ~ 0
COM1
Text Label 3275 2900 2    60   ~ 0
CLMP1
Text Label 3275 2700 2    60   ~ 0
BOOT1
Text Label 4950 3300 0    60   ~ 0
ILIM
Text Label 6675 2650 0    60   ~ 0
ILIM
$Comp
L GND #PWR053
U 1 1 5B0D321D
P 6100 2675
F 0 "#PWR053" H 6100 2425 50  0001 C CNN
F 1 "GND" H 6100 2525 50  0000 C CNN
F 2 "" H 6100 2675 50  0000 C CNN
F 3 "" H 6100 2675 50  0000 C CNN
	1    6100 2675
	1    0    0    -1  
$EndComp
$Comp
L R_Small R201
U 1 1 5B0D36D8
P 5475 4150
F 0 "R201" H 5505 4170 50  0000 L CNN
F 1 "DNP" H 5505 4110 50  0000 L CNN
F 2 "RCL:0603_RES" H 5475 4150 50  0001 C CNN
F 3 "" H 5475 4150 50  0000 C CNN
F 4 "DNP" H 5475 4150 60  0001 C CNN "Placement"
	1    5475 4150
	1    0    0    1   
$EndComp
$Comp
L C_Small C214
U 1 1 5B0D3B6A
P 5700 2975
F 0 "C214" H 5710 3045 50  0000 L CNN
F 1 "10u" H 5710 2895 50  0000 L CNN
F 2 "RCL:0603_CAP" H 5700 2975 50  0001 C CNN
F 3 "" H 5700 2975 50  0000 C CNN
F 4 "MURATA" H 5700 2975 60  0001 C CNN "manf"
F 5 "ZRB18AR61E106ME01L" H 5700 2975 60  0001 C CNN "MPN"
F 6 "16V" H 5700 2975 60  0001 C CNN "Remarks"
	1    5700 2975
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR054
U 1 1 5B0D3C4D
P 5700 3150
F 0 "#PWR054" H 5700 2900 50  0001 C CNN
F 1 "GND" H 5700 3000 50  0000 C CNN
F 2 "" H 5700 3150 50  0000 C CNN
F 3 "" H 5700 3150 50  0000 C CNN
	1    5700 3150
	1    0    0    -1  
$EndComp
$Comp
L bq25071 U202
U 1 1 5B0D4962
P 8350 2450
AR Path="/5B0D4962" Ref="U202"  Part="1" 
AR Path="/5B041AA4/5B0D4962" Ref="U202"  Part="1" 
F 0 "U202" H 8350 3100 60  0000 C CNN
F 1 "bq25071" H 8350 3000 60  0000 C CNN
F 2 "Housings_DFN_QFN:DFN-10-1EP_2x3mm_Pitch0.5mm" H 8500 2300 60  0001 C CNN
F 3 "" H 8500 2300 60  0001 C CNN
F 4 "BQ25071DQCT" H 8350 2450 60  0001 C CNN "MPN"
	1    8350 2450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P201
U 1 1 5B0D507B
P 10950 2725
F 0 "P201" H 10950 2925 50  0000 C CNN
F 1 "B3B-XH-A (LF)(SN)" V 11050 2725 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B03B-XH-A_03x2.50mm_Straight" H 10950 2725 50  0001 C CNN
F 3 "" H 10950 2725 50  0000 C CNN
F 4 "B3B-XH-A (LF)(SN)" H 10950 2725 60  0001 C CNN "MPN"
	1    10950 2725
	1    0    0    -1  
$EndComp
Text Label 9375 2725 2    60   ~ 0
TS
Text Label 7700 2550 2    60   ~ 0
TS
$Comp
L GND #PWR055
U 1 1 5B0D543E
P 10750 2925
F 0 "#PWR055" H 10750 2675 50  0001 C CNN
F 1 "GND" H 10750 2775 50  0000 C CNN
F 2 "" H 10750 2925 50  0000 C CNN
F 3 "" H 10750 2925 50  0000 C CNN
	1    10750 2925
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR056
U 1 1 5B0D55CC
P 8350 3250
F 0 "#PWR056" H 8350 3000 50  0001 C CNN
F 1 "GND" H 8350 3100 50  0000 C CNN
F 2 "" H 8350 3250 50  0000 C CNN
F 3 "" H 8350 3250 50  0000 C CNN
	1    8350 3250
	1    0    0    -1  
$EndComp
$Comp
L R_Small R208
U 1 1 5B0D5C87
P 9800 925
F 0 "R208" V 9725 875 50  0000 L CNN
F 1 "0" V 9800 900 50  0000 L CNN
F 2 "RCL:0603_RES" H 9800 925 50  0001 C CNN
F 3 "" H 9800 925 50  0000 C CNN
F 4 "ERJ-3GEY0R00V" H 9800 925 60  0001 C CNN "MPN"
	1    9800 925 
	0    1    1    0   
$EndComp
Text Notes 10525 1225 2    40   ~ 0
Optionally replace R208 by a diode \nto ensure VBAT<3.6V (in the case \nof a different battery/charge method)
Text Notes 7150 675  0    50   ~ 0
Optional place instead of charger
Text Notes 7225 1575 0    50   ~ 0
LiFePO4 charger
Text Notes 11275 2850 1    40   ~ 0
LiFePO4 \nbattery
$Comp
L C_Small C217
U 1 1 5B0D664D
P 9025 2575
F 0 "C217" H 9035 2645 50  0000 L CNN
F 1 "100n" H 9035 2495 50  0000 L CNN
F 2 "RCL:0603_CAP" H 9025 2575 50  0001 C CNN
F 3 "" H 9025 2575 50  0000 C CNN
	1    9025 2575
	1    0    0    -1  
$EndComp
$Comp
L C_Small C216
U 1 1 5B0D684D
P 9000 1850
F 0 "C216" H 9010 1920 50  0000 L CNN
F 1 "4.7u" H 9010 1770 50  0000 L CNN
F 2 "RCL:0603_CAP" H 9000 1850 50  0001 C CNN
F 3 "" H 9000 1850 50  0000 C CNN
	1    9000 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR057
U 1 1 5B0D6A39
P 9000 1950
F 0 "#PWR057" H 9000 1700 50  0001 C CNN
F 1 "GND" H 9000 1800 50  0000 C CNN
F 2 "" H 9000 1950 50  0000 C CNN
F 3 "" H 9000 1950 50  0000 C CNN
	1    9000 1950
	1    0    0    -1  
$EndComp
$Comp
L R_Small R209
U 1 1 5B0D75E8
P 9625 2575
F 0 "R209" H 9675 2575 50  0000 L CNN
F 1 "100k" V 9550 2475 50  0000 L CNN
F 2 "RCL:0603_RES" H 9625 2575 50  0001 C CNN
F 3 "" H 9625 2575 50  0000 C CNN
F 4 "CRGH0603J100K" H 9625 2575 60  0001 C CNN "MPN"
	1    9625 2575
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR058
U 1 1 5B0D7B92
P 9625 3025
F 0 "#PWR058" H 9625 2775 50  0001 C CNN
F 1 "GND" H 9625 2875 50  0000 C CNN
F 2 "" H 9625 3025 50  0000 C CNN
F 3 "" H 9625 3025 50  0000 C CNN
	1    9625 3025
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR059
U 1 1 5B0D87BB
P 9025 2750
F 0 "#PWR059" H 9025 2500 50  0001 C CNN
F 1 "GND" H 9025 2600 50  0000 C CNN
F 2 "" H 9025 2750 50  0000 C CNN
F 3 "" H 9025 2750 50  0000 C CNN
	1    9025 2750
	1    0    0    -1  
$EndComp
$Comp
L C_Small C215
U 1 1 5B0D8B70
P 7575 1825
F 0 "C215" H 7585 1895 50  0000 L CNN
F 1 "100n" H 7585 1745 50  0000 L CNN
F 2 "RCL:0603_CAP" H 7575 1825 50  0001 C CNN
F 3 "" H 7575 1825 50  0000 C CNN
	1    7575 1825
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR060
U 1 1 5B0D8C43
P 7575 1950
F 0 "#PWR060" H 7575 1700 50  0001 C CNN
F 1 "GND" H 7575 1800 50  0000 C CNN
F 2 "" H 7575 1950 50  0000 C CNN
F 3 "" H 7575 1950 50  0000 C CNN
	1    7575 1950
	1    0    0    -1  
$EndComp
$Comp
L R_Small R210
U 1 1 5B0D95F8
P 9625 2875
F 0 "R210" H 9675 2875 50  0000 L CNN
F 1 "20k" V 9550 2775 50  0000 L CNN
F 2 "RCL:0603_RES" H 9625 2875 50  0001 C CNN
F 3 "" H 9625 2875 50  0000 C CNN
	1    9625 2875
	1    0    0    -1  
$EndComp
Text Notes 9975 3600 2    40   ~ 0
TS monitoring disabled. \nAlternatively change R209 \nand R210 according to \n"PCB calculations.ipynb" 
$Comp
L R_Small R202
U 1 1 5B0DC938
P 2575 4275
F 0 "R202" H 2605 4295 50  0000 L CNN
F 1 "10k" H 2605 4235 50  0000 L CNN
F 2 "RCL:0603_RES" H 2575 4275 50  0001 C CNN
F 3 "" H 2575 4275 50  0000 C CNN
	1    2575 4275
	-1   0    0    1   
$EndComp
$Comp
L R_Small R207
U 1 1 5B0DCF35
P 9075 4175
F 0 "R207" H 9105 4195 50  0000 L CNN
F 1 "10k" H 9105 4135 50  0000 L CNN
F 2 "RCL:0603_RES" H 9075 4175 50  0001 C CNN
F 3 "" H 9075 4175 50  0000 C CNN
	1    9075 4175
	1    0    0    1   
$EndComp
$Comp
L R_Small R206
U 1 1 5B0DF65B
P 7500 2675
F 0 "R206" V 7425 2625 50  0000 L CNN
F 1 "2k4" V 7575 2600 50  0000 L CNN
F 2 "RCL:0603_RES" H 7500 2675 50  0001 C CNN
F 3 "" H 7500 2675 50  0000 C CNN
F 4 "MCWR06X2401FTL" H 7500 2675 60  0001 C CNN "MPN"
	1    7500 2675
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR061
U 1 1 5B0DF9E7
P 7500 2825
F 0 "#PWR061" H 7500 2575 50  0001 C CNN
F 1 "GND" H 7500 2675 50  0000 C CNN
F 2 "" H 7500 2825 50  0000 C CNN
F 3 "" H 7500 2825 50  0000 C CNN
	1    7500 2825
	1    0    0    -1  
$EndComp
Text Notes 7100 3600 0    50   ~ 0
Programmed at \n240mA charge \ncurrent with R206
Text HLabel 3425 5025 3    50   Input ~ 0
Qi_EN1
Text HLabel 4850 5025 3    50   Input ~ 0
Qi_EN2
Text HLabel 2900 5025 3    50   Output ~ 0
Qi_CHG
Text HLabel 7825 5050 3    50   Input ~ 0
CHARGER_EN
Text HLabel 8875 5050 3    50   Output ~ 0
CHARGER_CHG
Text HLabel 10450 4025 2    50   Input ~ 0
VDD_PULL_UP
Text HLabel 10550 925  2    50   Output ~ 0
VBAT_OUT
$Comp
L C_Small C209
U 1 1 5B0EE32A
P 2000 2850
F 0 "C209" V 1950 2900 50  0000 L CNN
F 1 "10n" V 1950 2650 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 2850 50  0001 C CNN
F 3 "" H 2000 2850 50  0000 C CNN
F 4 "25V" H 2000 2850 60  0001 C CNN "Remarks"
	1    2000 2850
	0    1    -1   0   
$EndComp
$Comp
L C_Small C210
U 1 1 5B0EE331
P 2000 3075
F 0 "C210" V 1950 3125 50  0000 L CNN
F 1 "0.47u" V 1950 2825 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 3075 50  0001 C CNN
F 3 "" H 2000 3075 50  0000 C CNN
F 4 "25V" H 2000 3075 60  0001 C CNN "Remarks"
	1    2000 3075
	0    1    -1   0   
$EndComp
$Comp
L C_Small C211
U 1 1 5B0EE338
P 2000 3300
F 0 "C211" V 1950 3350 50  0000 L CNN
F 1 "22n" V 1950 3125 50  0000 L CNN
F 2 "RCL:0603_CAP" H 2000 3300 50  0001 C CNN
F 3 "" H 2000 3300 50  0000 C CNN
F 4 "25V" H 2000 3300 60  0001 C CNN "Remarks"
	1    2000 3300
	0    1    -1   0   
$EndComp
$Comp
L NTC TH201
U 1 1 5B0F00DC
P 5475 4500
F 0 "TH201" V 5575 4550 50  0000 C CNN
F 1 "NTC" V 5375 4500 50  0000 C BNN
F 2 "RCL:0603_RES" H 5475 4500 50  0001 C CNN
F 3 "" H 5475 4500 50  0000 C CNN
F 4 "DNP" H 5475 4500 60  0001 C CNN "Placement"
	1    5475 4500
	1    0    0    -1  
$EndComp
$Comp
L R_Small R211
U 1 1 5B0F0196
P 5150 4475
F 0 "R211" H 5180 4495 50  0000 L CNN
F 1 "10k" H 5180 4435 50  0000 L CNN
F 2 "RCL:0603_RES" H 5150 4475 50  0001 C CNN
F 3 "" H 5150 4475 50  0000 C CNN
	1    5150 4475
	1    0    0    1   
$EndComp
$Comp
L GND #PWR062
U 1 1 5B0F095D
P 5475 4850
F 0 "#PWR062" H 5475 4600 50  0001 C CNN
F 1 "GND" H 5475 4700 50  0000 C CNN
F 2 "" H 5475 4850 50  0000 C CNN
F 3 "" H 5475 4850 50  0000 C CNN
	1    5475 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2500 5125 2500
Wire Wire Line
	675  1950 675  1900
Wire Wire Line
	675  1900 1150 1900
Wire Wire Line
	675  2550 675  2600
Wire Wire Line
	675  2600 2275 2600
Wire Wire Line
	1475 1900 1475 2150
Wire Wire Line
	3275 2600 3500 2600
Connection ~ 1475 1900
Wire Wire Line
	3300 3300 3500 3300
Wire Wire Line
	2950 925  8100 925 
Wire Wire Line
	2950 2125 3025 2125
Wire Wire Line
	4800 2700 5900 2700
Wire Wire Line
	5500 2700 5500 2875
Wire Wire Line
	5500 3075 5500 3150
Wire Wire Line
	3225 2125 3300 2125
Connection ~ 5500 2700
Wire Wire Line
	1475 2350 1475 2600
Connection ~ 1475 2600
Wire Wire Line
	975  1900 975  2050
Connection ~ 975  1900
Wire Wire Line
	975  2450 975  2600
Connection ~ 975  2600
Wire Wire Line
	1350 1900 2275 1900
Wire Wire Line
	1700 1200 1700 2150
Connection ~ 1700 1900
Wire Wire Line
	1700 2350 1700 3300
Connection ~ 1700 2600
Wire Wire Line
	1900 1650 1700 1650
Wire Wire Line
	1700 1425 1900 1425
Connection ~ 1700 1650
Wire Wire Line
	1700 1200 1900 1200
Connection ~ 1700 1425
Wire Wire Line
	2100 1200 2275 1200
Wire Wire Line
	2100 1425 2275 1425
Wire Wire Line
	2100 1650 2275 1650
Connection ~ 1700 2850
Connection ~ 1700 3075
Wire Wire Line
	3275 2900 3500 2900
Wire Wire Line
	4950 2900 4800 2900
Wire Wire Line
	3275 3000 3500 3000
Wire Wire Line
	4950 3000 4800 3000
Wire Wire Line
	3275 2700 3500 2700
Wire Wire Line
	4950 2800 4800 2800
Wire Wire Line
	1350 1575 1425 1575
Wire Wire Line
	1425 1275 1425 1900
Connection ~ 1425 1900
Wire Wire Line
	1350 1275 1425 1275
Connection ~ 1425 1575
Wire Wire Line
	1150 1275 1100 1275
Wire Wire Line
	1100 1275 1100 1900
Connection ~ 1100 1900
Wire Wire Line
	1150 1575 1100 1575
Connection ~ 1100 1575
Wire Wire Line
	4950 3100 4800 3100
Wire Wire Line
	4800 3300 4950 3300
Wire Wire Line
	5700 2400 6675 2400
Wire Wire Line
	6400 2400 6400 2450
Connection ~ 6400 2400
Wire Wire Line
	6400 2650 6675 2650
Wire Wire Line
	6100 2675 6100 2650
Wire Wire Line
	4800 3200 5150 3200
Wire Wire Line
	5150 3200 5150 4375
Wire Wire Line
	5700 2650 5700 2875
Connection ~ 5700 2700
Wire Wire Line
	5700 3075 5700 3150
Wire Wire Line
	5700 2150 7850 2150
Connection ~ 5700 925 
Wire Wire Line
	8850 2150 10525 2150
Wire Wire Line
	9325 2150 9325 925 
Wire Wire Line
	8400 925  9700 925 
Connection ~ 9325 2150
Wire Wire Line
	9375 2725 10075 2725
Wire Wire Line
	8850 2250 10700 2250
Wire Wire Line
	8350 3150 8350 3250
Wire Wire Line
	10750 2825 10750 2825
Wire Wire Line
	10750 2825 10750 2925
Connection ~ 9325 925 
Wire Wire Line
	9900 925  10550 925 
Wire Notes Line
	7125 1400 7125 600 
Wire Notes Line
	7125 600  9000 600 
Wire Notes Line
	9000 600  9000 1400
Wire Notes Line
	9000 1400 7125 1400
Wire Wire Line
	9000 1700 9000 1750
Wire Wire Line
	8875 1700 9000 1700
Wire Wire Line
	8875 2150 8875 1700
Connection ~ 8875 2150
Wire Wire Line
	9625 2675 9625 2775
Connection ~ 9625 2725
Wire Wire Line
	9625 2975 9625 3025
Wire Wire Line
	9625 2450 9625 2475
Wire Wire Line
	8850 2450 9625 2450
Wire Wire Line
	9025 2475 9025 2450
Connection ~ 9025 2450
Wire Wire Line
	9025 2750 9025 2675
Wire Wire Line
	7575 1950 7575 1925
Wire Wire Line
	7575 1725 7575 1675
Wire Wire Line
	7575 1675 7800 1675
Wire Wire Line
	7800 1675 7800 2150
Connection ~ 7800 2150
Wire Wire Line
	7825 2650 7825 5050
Wire Wire Line
	8875 2650 8875 5050
Wire Wire Line
	7825 2650 7850 2650
Wire Wire Line
	3500 3400 3425 3400
Wire Wire Line
	3425 3400 3425 5025
Wire Wire Line
	4800 3400 4850 3400
Wire Wire Line
	4850 3400 4850 5025
Wire Wire Line
	2900 3100 2900 5025
Wire Wire Line
	9075 4075 9075 4025
Wire Wire Line
	9075 4275 9075 4375
Wire Wire Line
	9075 4375 8875 4375
Connection ~ 8875 4375
Wire Wire Line
	7700 2550 7850 2550
Wire Wire Line
	7850 2450 7500 2450
Wire Wire Line
	7500 2450 7500 2575
Wire Wire Line
	7500 2825 7500 2775
Wire Wire Line
	8875 2650 8850 2650
Connection ~ 10525 2250
Wire Wire Line
	8200 3200 8500 3200
Wire Wire Line
	8500 3200 8500 3150
Wire Wire Line
	8200 3200 8200 3150
Connection ~ 8350 3200
Wire Notes Line
	7075 1475 10000 1475
Wire Notes Line
	10000 1475 10000 3625
Wire Notes Line
	10000 3625 7075 3625
Wire Notes Line
	7075 3625 7075 1475
Wire Wire Line
	1900 2850 1700 2850
Wire Wire Line
	1700 3075 1900 3075
Wire Wire Line
	1700 3300 1900 3300
Wire Wire Line
	2100 3300 2275 3300
Wire Wire Line
	2100 3075 2275 3075
Wire Wire Line
	2100 2850 2275 2850
Wire Wire Line
	5475 4700 5475 4850
Wire Wire Line
	5150 4000 5975 4000
Wire Wire Line
	5475 4000 5475 4050
Wire Wire Line
	5475 4250 5475 4300
Wire Wire Line
	5475 4725 5150 4725
Wire Wire Line
	5150 4725 5150 4575
Connection ~ 5475 4725
Connection ~ 5150 4000
Wire Wire Line
	5975 4000 5975 4650
Connection ~ 5475 4000
Wire Wire Line
	5975 4650 10350 4650
Wire Notes Line
	5275 3950 5725 3950
Wire Notes Line
	5725 3950 5725 5100
Wire Notes Line
	5725 5100 5275 5100
Wire Notes Line
	5275 5100 5275 3950
$Comp
L R_Small R213
U 1 1 5B0F464B
P 10175 2725
F 0 "R213" H 10225 2725 50  0000 L CNN
F 1 "0" V 10100 2700 50  0000 L CNN
F 2 "RCL:0603_RES" H 10175 2725 50  0001 C CNN
F 3 "" H 10175 2725 50  0000 C CNN
F 4 "DNP" H 10175 2725 60  0001 C CNN "Placement"
	1    10175 2725
	0    1    1    0   
$EndComp
$Comp
L R_Small R214
U 1 1 5B0F46FC
P 10350 2875
F 0 "R214" H 10400 2875 50  0000 L CNN
F 1 "R" V 10275 2775 50  0000 L CNN
F 2 "RCL:0603_RES" H 10350 2875 50  0001 C CNN
F 3 "" H 10350 2875 50  0000 C CNN
F 4 "DNP" H 10350 2875 60  0001 C CNN "Placement"
	1    10350 2875
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 2725 10350 2775
Wire Wire Line
	10275 2725 10750 2725
Connection ~ 10350 2725
Wire Wire Line
	10350 4650 10350 2975
Text Notes 5250 5650 0    50   ~ 0
Current configuration: no temperature sensor\n\nOptional onboard temperature sensor\nR214 should be removed in that configuration\n\nSee design document for configuration table
$Comp
L R_Small R212
U 1 1 5B0F8675
P 5700 2550
F 0 "R212" H 5725 2525 50  0000 L CNN
F 1 "20k" H 5725 2600 50  0000 L CNN
F 2 "RCL:0603_RES" H 5700 2550 50  0001 C CNN
F 3 "" H 5700 2550 50  0000 C CNN
F 4 "MCMR06X2002FTL" H 5700 2550 60  0001 C CNN "MPN"
	1    5700 2550
	1    0    0    -1  
$EndComp
Connection ~ 5700 2150
Wire Wire Line
	5700 2375 5700 2450
Wire Wire Line
	6100 2400 6100 2450
Connection ~ 5700 2400
Connection ~ 6100 2400
Text Notes 5575 2050 0    50   ~ 0
Current Limit \nset to 260 mA
$Comp
L C_Small C219
U 1 1 5B0FAF85
P 5900 2975
F 0 "C219" H 5910 3045 50  0000 L CNN
F 1 "10u" H 5910 2895 50  0000 L CNN
F 2 "RCL:0603_CAP" H 5900 2975 50  0001 C CNN
F 3 "" H 5900 2975 50  0000 C CNN
F 4 "DNP" H 5900 2975 60  0001 C CNN "Placement"
F 5 "16V" H 5900 2975 60  0001 C CNN "Remarks"
	1    5900 2975
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR063
U 1 1 5B0FAF8B
P 5900 3150
F 0 "#PWR063" H 5900 2900 50  0001 C CNN
F 1 "GND" H 5900 3000 50  0000 C CNN
F 2 "" H 5900 3150 50  0000 C CNN
F 3 "" H 5900 3150 50  0000 C CNN
	1    5900 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2700 5900 2875
Wire Wire Line
	5900 3075 5900 3150
$Comp
L C_Small C218
U 1 1 5B0FB38C
P 3125 2275
F 0 "C218" H 3000 2350 50  0000 L CNN
F 1 "100n" H 2925 2225 50  0000 L CNN
F 2 "RCL:0603_CAP" H 3125 2275 50  0001 C CNN
F 3 "" H 3125 2275 50  0000 C CNN
	1    3125 2275
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 2275 3025 2275
Wire Wire Line
	3225 2275 3250 2275
Wire Wire Line
	3250 2275 3250 2125
Connection ~ 3250 2125
$Comp
L ZENERsmall D202
U 1 1 5B104C2F
P 7950 1075
F 0 "D202" H 7950 1175 50  0000 C CNN
F 1 "D" H 7950 975 50  0000 C CNN
F 2 "Custom:SOD-123_custom" H 7950 1075 50  0001 C CNN
F 3 "" H 7950 1075 50  0000 C CNN
F 4 "DNP" H 7950 1075 60  0001 C CNN "Placement"
	1    7950 1075
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 975  7950 925 
Connection ~ 7950 925 
$Comp
L GND #PWR064
U 1 1 5B104E24
P 7950 1200
F 0 "#PWR064" H 7950 950 50  0001 C CNN
F 1 "GND" H 7950 1050 50  0000 C CNN
F 2 "" H 7950 1200 50  0000 C CNN
F 3 "" H 7950 1200 50  0000 C CNN
	1    7950 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1200 7950 1175
$Comp
L D D203
U 1 1 5B11B669
P 9825 675
F 0 "D203" H 9825 775 50  0000 C CNN
F 1 "D" H 9825 575 50  0000 C CNN
F 2 "Custom:SOD-123_custom" H 9825 675 50  0001 C CNN
F 3 "" H 9825 675 50  0000 C CNN
F 4 "MBR0520LT1G" H 9825 675 60  0001 C CNN "MPN"
F 5 "DNP" H 9825 675 60  0001 C CNN "Placement"
F 6 "MBR0520LT1G diode can be placed for li-ion (max 4.2V charge voltage)" H 9825 675 60  0001 C CNN "Remarks"
	1    9825 675 
	-1   0    0    1   
$EndComp
Wire Wire Line
	9675 675  9575 675 
Wire Wire Line
	9575 675  9575 925 
Connection ~ 9575 925 
Wire Wire Line
	9975 675  10125 675 
Wire Wire Line
	10125 675  10125 925 
Connection ~ 10125 925 
Wire Wire Line
	10525 2150 10525 2250
$Comp
L F_Small F201
U 1 1 5B136971
P 10700 2450
F 0 "F201" H 10660 2510 50  0000 L CNN
F 1 "500mA" H 10580 2390 50  0000 L CNN
F 2 "RCL:0603_GENERAL" H 10700 2450 50  0001 C CNN
F 3 "" H 10700 2450 50  0000 C CNN
F 4 "Bourns" H 10700 2450 60  0001 C CNN "manf"
F 5 "SF-0603S050" H 10700 2450 60  0001 C CNN "MPN"
F 6 "I^2s > 0.006 A^2s @ 200mohm fuse ESR" H 10700 2450 60  0001 C CNN "Remarks"
	1    10700 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	10700 2250 10700 2350
Wire Wire Line
	10700 2550 10700 2625
Wire Wire Line
	10700 2625 10750 2625
$Comp
L BQ51013BRHLR U201
U 1 1 5B1EE574
P 4100 3000
F 0 "U201" H 4100 3000 50  0000 L BNN
F 1 "BQ51013BRHLR" H 3850 3650 50  0000 L BNN
F 2 "Custom:QFN-20(VQFN-20)" H 3850 2400 50  0001 L BNN
F 3 "" H 4450 2250 50  0001 L BNN
F 4 "BQ51013BRHL" H 4100 3000 60  0001 C CNN "MPN"
	1    4100 3000
	1    0    0    -1  
$EndComp
NoConn ~ 3500 3200
Text Label 2275 2600 0    60   ~ 0
AC2
Text Label 4950 2600 0    60   ~ 0
AC2
Wire Wire Line
	4950 2600 4800 2600
Text Label 2275 1900 0    60   ~ 0
AC1
Text Label 3275 2600 2    60   ~ 0
AC1
Wire Wire Line
	2950 2800 3500 2800
Wire Wire Line
	2950 925  2950 2800
Connection ~ 2950 2275
Connection ~ 2950 2125
Wire Wire Line
	3500 3100 2900 3100
$Comp
L GND #PWR065
U 1 1 5B1F33B3
P 4150 3775
F 0 "#PWR065" H 4150 3525 50  0001 C CNN
F 1 "GND" H 4150 3625 50  0000 C CNN
F 2 "" H 4150 3775 50  0000 C CNN
F 3 "" H 4150 3775 50  0000 C CNN
	1    4150 3775
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3775 4150 3700
$Comp
L GND #PWR066
U 1 1 5B1F4501
P 3425 2500
F 0 "#PWR066" H 3425 2250 50  0001 C CNN
F 1 "GND" H 3425 2350 50  0000 C CNN
F 2 "" H 3425 2500 50  0000 C CNN
F 3 "" H 3425 2500 50  0000 C CNN
	1    3425 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	3425 2500 3500 2500
Wire Wire Line
	2575 4375 2575 4550
Wire Wire Line
	2575 4550 2900 4550
Connection ~ 2900 4550
Wire Wire Line
	2575 4175 2575 3975
Wire Wire Line
	9075 4025 10450 4025
Text Label 9375 4025 0    60   ~ 0
VDD_PULL_UP
Text Label 2575 3975 1    60   ~ 0
VDD_PULL_UP
Wire Wire Line
	5700 925  5700 2175
Text Label 3300 925  0    60   ~ 0
Qi_5V
$EndSCHEMATC
